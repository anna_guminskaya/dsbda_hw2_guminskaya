#!/usr/bin/env bash
if [[ $# -eq 0 ]] ; then
    echo 'You should specify database name!'
    exit 1
fi

echo "Creating .jar file..."
mvn package -f ../pom.xml

docker cp ../target/homework2-1.0-SNAPSHOT-jar-with-dependencies.jar $1:/tmp
docker cp generateInputData.sh $1:/

echo "Go to container with 'docker exec -it container_name bash' command and start ./generateInputData.sh in it"
