package guminskaya.dsbda.homework2;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author Anna Guminskaya
 * @since 08/11/2018.
 */
@Data
@AllArgsConstructor
public class LogLevelHour {

    // Уровень логирования
    private String logLevel;

    // Час, в который произошло событие
    private int hour;
}
